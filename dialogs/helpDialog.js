const { AttachmentLayoutTypes, CardFactory } = require("botbuilder");
const {
  ComponentDialog,
  DialogTurnStatus,
  WaterfallDialog,
} = require("botbuilder-dialogs");

const WATERFALL_DIALOG = "mainWaterfallDialog";

class HelpDialog extends ComponentDialog {
  constructor() {
    super("HelpDialog");

    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.helpChoicesStep.bind(this),
        this.helpSelectionStep.bind(this),
      ])
    );

    this.initialDialogId = WATERFALL_DIALOG;
  }

  async helpChoicesStep(stepContext) {
    var promptMessage;
    if (stepContext.options.retry)
      promptMessage = "Sorry, please click a button from the menus for help 😵";
    else if (stepContext.options.moreHelp)
      promptMessage = "Is there anything else that I can help you with?";
    else
      promptMessage =
        "Want to learn more about OurVoice x Facebook and explore the frequently asked questions from other users? Click a button from the menu below for more information!";
    await stepContext.context.sendActivity(promptMessage);

    await stepContext.context.sendActivity({
      attachments: this.helpChoices(),
      attachmentLayout: AttachmentLayoutTypes.Carousel,
    });
    return { status: DialogTurnStatus.waiting };
  }

  async helpSelectionStep(stepContext) {
    const helpSelected = stepContext.result;
    if (!helpSelected)
      return await stepContext.replaceDialog(this.initialDialogId, {
        retry: true,
      });

    switch (helpSelected) {
      case "About":
        await stepContext.context.sendActivity(
          "OurVoice x Facebook is a secure anonymous communication platform developed by Peter Chen of the Faculty of Information Technology, Monash University. OurVoice x Facebook is completely anonymous (see the FAQ for details or mail peter.chen@monash.edu) and all posts and comments are anonymously moderated before publishing to the Facebook page."
        );
        break;

      case "Cancel":
        await stepContext.context.sendActivity(
          "Sure, chat with me again if you have any further questions."
        );
        return await stepContext.endDialog();

      case "FAQ1":
        await stepContext.context.sendActivity(
          "All connections to OurVoice x Facebook are made using the HTTPS protocol. Although people will be using their personal Messenger account for making anonymous posts or comments, the account and personal details will not be recorded for moderation and publishing. Also, all the moderated contents will be posted by the Anonymous Bot on behalf of the content owners without revealing the real user identity."
        );
        break;

      case "FAQ2":
        const faq2 = [
          "Users of OurVoice x Facebook are asked to follow our code of conduct:\r\n\n(a) Truthfulness and Honesty. OurVoice x Facebook is not intended to be a rumour mill. Please ensure that you have good reason to believe that what you are posting is actually true. Posts based upon first-hand experience, documentary evidence, or material that you have heard from multiple reliable sources are more likely to be true.",
          "(b) Being Constructive. When writing a post, please consider carefully how it might contribute to a discussion. Posts that are unnecessarily adversarial will be edited or rejected. It is also important to consider the wider consequences of what you are saying. Facebook page is in practice a public discussion platform, so what is published could have direct implications for other members of the community.",
          "(c) Anonymity. A poorly drafted post risks exposing the identity of the author. Even if you are not concerned about your identity being revealed, OurVoice x Facebook is intended to be an anonymous platform for communication. Phrasing, names, dates and references to small groups of people or specific places can all provide clues to the identity of the author and risk jigsaw identification. For example, rather than stating that “I am a tutor for the IT for World Domination unit and was told by Dr Strange to...”, instead say that “I know a tutor for a masters unit who was told by their CE to...”. The latter does not reference a specific unit or person but still makes the point.",
          "Following these guidelines will help ensure that your posts are published without any modification by the moderators; although you can be reassured that the moderators will act to protect your identity too. Only the volunteer moderators will have access to unmoderated posts. All original versions of moderated posts, as well as rejected posts, are permanently deleted.",
        ];
        for (let i = 0; i < faq2.length; i++)
          await stepContext.context.sendActivity(faq2[i]);
        break;

      case "FAQ3":
        await stepContext.context.sendActivity(
          "Newly posted messages are queued for moderation and are not immediately displayed. Batches of moderated messages are posted two-times-a-day at 9am and 5pm. If posts are published (either as originally posted or in a modified form agreed on by the moderators), then any other user of OurVoice x Facebook can read them and comment on them. While there is no way that users of OurVoice x Facebook can be prevented from re-publishing messages and comments (for example, on other social media platforms) we strongly recommend that people do not do this."
        );
        break;

      case "FAQ3":
        const faq4 = [
          "All posts (messages or comments) are moderated before they are made available on the Facebook page, both to ensure constructive discussion and to minimise the risk that users accidentally post information that identifies themselves or others. Moderators can do one of three things:\r\n\n(1) Accept a post without any changes;",
          "(2) Modify a post according to our Code of Conduct;",
          "(3) Reject a post, in which case the post will be permanently deleted.",
          "New (moderated) messages and comments are released through OurVoice x Facebook two-times-a-day at 9am and 5pm. Messages and comments are moderated independently by the moderators, and if any disagreements arise (eg. edits or rejections), they will be resolved by the moderation team via video conferencing. Posts (or edited posts) on which moderators cannot agree will be rejected. As all posts in OurVoice x Facebook are anonymous, moderators have no way to identify the authors of messages or comments.",
        ];
        for (let i = 0; i < faq4.length; i++)
          await stepContext.context.sendActivity(faq4[i]);
        break;

      default:
        return await stepContext.replaceDialog(this.initialDialogId, {
          retry: true,
        });
    }

    return await stepContext.replaceDialog(this.initialDialogId, {
      moreHelp: true,
    });
  }

  // Helper functions used to create cards.
  helpChoices() {
    const helpSeriesOptions = [
      CardFactory.heroCard(
        "About OurVoice",
        "Learn more about OurVoice x Facebook.",
        CardFactory.images([
          "https://firebasestorage.googleapis.com/v0/b/wechaty-demo.appspot.com/o/Images%2FFacebook.jpg?alt=media&token=ab3da1f9-50ab-4d83-82cf-3a344ae03007",
        ]),
        CardFactory.actions([
          {
            type: "imBack",
            title: "About",
            value: "About",
          },
        ])
      ),

      CardFactory.heroCard(
        "Cancel",
        "Terminate the help session and explore more features for OurVoice.",
        CardFactory.images([
          "https://firebasestorage.googleapis.com/v0/b/wechaty-demo.appspot.com/o/Images%2FCancel.jpg?alt=media&token=f4187ded-cf4c-4f2d-98e3-23e0e0ff3f76",
        ]),
        CardFactory.actions([
          {
            type: "imBack",
            title: "Cancel",
            value: "Cancel",
          },
        ])
      ),

      CardFactory.heroCard(
        "FAQ",
        "How can OurVoice ensure anonymity?",
        CardFactory.images([
          "https://firebasestorage.googleapis.com/v0/b/wechaty-demo.appspot.com/o/Images%2FFAQ.jpg?alt=media&token=83d9acd6-7209-4258-8fbe-97d2d95eb1d1",
        ]),
        CardFactory.actions([
          {
            type: "imBack",
            title: "Find Answer",
            value: "FAQ1",
          },
        ])
      ),

      CardFactory.heroCard(
        "FAQ",
        "Are there any restrictions on posts?",
        CardFactory.images([
          "https://firebasestorage.googleapis.com/v0/b/wechaty-demo.appspot.com/o/Images%2FFAQ.jpg?alt=media&token=83d9acd6-7209-4258-8fbe-97d2d95eb1d1",
        ]),
        CardFactory.actions([
          {
            type: "imBack",
            title: "Find Answer",
            value: "FAQ2",
          },
        ])
      ),

      CardFactory.heroCard(
        "FAQ",
        "Who can see my posts and comments?",
        CardFactory.images([
          "https://firebasestorage.googleapis.com/v0/b/wechaty-demo.appspot.com/o/Images%2FFAQ.jpg?alt=media&token=83d9acd6-7209-4258-8fbe-97d2d95eb1d1",
        ]),
        CardFactory.actions([
          {
            type: "imBack",
            title: "Find Answer",
            value: "FAQ3",
          },
        ])
      ),

      CardFactory.heroCard(
        "FAQ",
        "When will my messages posted?",
        CardFactory.images([
          "https://firebasestorage.googleapis.com/v0/b/wechaty-demo.appspot.com/o/Images%2FFAQ.jpg?alt=media&token=83d9acd6-7209-4258-8fbe-97d2d95eb1d1",
        ]),
        CardFactory.actions([
          {
            type: "imBack",
            title: "Find Answer",
            value: "FAQ4",
          },
        ])
      ),
    ];

    return helpSeriesOptions;
  }
}

module.exports.HelpDialog = HelpDialog;
