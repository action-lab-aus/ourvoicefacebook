const {
  ComponentDialog,
  TextPrompt,
  WaterfallDialog,
} = require("botbuilder-dialogs");
const { getLocalDateTime } = require("../utils/timeFormatter");
const { verifyUser, postUser } = require("../apiGateway/apiGateway");

const WATERFALL_DIALOG = "WATERFALL_DIALOG";

class RegDialog extends ComponentDialog {
  constructor() {
    super("RegDialog");

    this.addDialog(new TextPrompt("TextPrompt"));
    this.addDialog(new TextPrompt("EmailPrompt", this.emailValidator));

    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.initialStep.bind(this),
        this.verificationStep.bind(this),
      ])
    );

    this.initialDialogId = WATERFALL_DIALOG;
  }

  // Initial step for asking the user email address.
  async initialStep(stepContext) {
    var promptOptions = {
      retryPrompt:
        "Sorry, you have entered an invalid email address 😵 Could you please try again?",
    };

    if (stepContext.options.reprompt === true)
      promptOptions.prompt =
        "Sorry, it seems that you don't have access to OurVoice. Please share me with your organization email address if you think there is a mistake.";
    else if (stepContext.options.errReprompt === true)
      promptOptions.prompt =
        "Sorry, an unexpected error occurred when retrieving your details 😵 Could you please try again?";
    else
      promptOptions.prompt =
        "Ahoy! Thanks for subscribing OurVoice! Before you start, I need to verify your identity first. Could you please send me your organisation email address for identification?";

    return await stepContext.prompt("EmailPrompt", promptOptions);
  }

  // Verify the user email address against the whitelist.
  async verificationStep(stepContext) {
    const email = stepContext.result;
    stepContext.values.email = email;

    const verifyRes = await verifyUser(email);
    if (verifyRes.status === 200) {
      const user = {
        userId: stepContext.context.activity.from.id,
        regDate: getLocalDateTime(),
        email: email,
        name: verifyRes.data.name,
      };

      console.log("Post User Details :", user);
      const postRes = await postUser(user);
      if (postRes.status === 201) {
        await stepContext.context.sendActivity(
          `Great, thanks ${user.name}! I have successfully found your details and you can get access to OurVoice now. Chat with me again if you want to make anonymous posts or comments via OurVoice!`
        );
        return await stepContext.endDialog();
      }
      // Reprompt the user due to an API Error.
      else
        return await stepContext.replaceDialog(this.initialDialogId, {
          errReprompt: true,
        });
    } else {
      // Reprompt the user for a valid email address.
      return await stepContext.replaceDialog(this.initialDialogId, {
        reprompt: true,
      });
    }
  }

  async emailValidator(promptContext) {
    const str = promptContext.context.activity.text;
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (str.toLowerCase().match(regex)) return true;
    return false;
  }
}

module.exports.RegDialog = RegDialog;
