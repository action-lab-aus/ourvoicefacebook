const {
  ComponentDialog,
  TextPrompt,
  WaterfallDialog,
} = require("botbuilder-dialogs");
const { CardFactory } = require("botbuilder");
const { getIntentAndScore } = require("../luis/luisHelper");
const { PostDialog } = require("../dialogs/postDialog");
const { CommentDialog } = require("../dialogs/commentDialog");
const { HelpDialog } = require("../dialogs/helpDialog");

const WATERFALL_DIALOG = "WATERFALL_DIALOG";

class UserDialog extends ComponentDialog {
  constructor() {
    super("UserDialog");

    this.addDialog(new PostDialog());
    this.addDialog(new CommentDialog());
    this.addDialog(new HelpDialog());

    this.addDialog(new TextPrompt("TextPrompt"));

    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.initialStep.bind(this),
        this.checkIntentStep.bind(this),
      ])
    );

    this.initialDialogId = WATERFALL_DIALOG;
  }

  // Initial step for selecting the action to perform.
  async initialStep(stepContext) {
    var promptMessage;
    if (stepContext.options.retry) {
      promptMessage =
        "Sorry, I can't understand your message 😵 Can I say that again?";
    } else {
      promptMessage = "";
      const heroCard = CardFactory.heroCard(
        "Welcome to OurVoice!",
        "Make a post or comment to Facebook page anonymously.",
        CardFactory.images([
          "https://firebasestorage.googleapis.com/v0/b/wechaty-demo.appspot.com/o/Images%2FAnonymous.png?alt=media&token=41df1eee-e1a4-4e4e-9551-a143d54ad319",
        ]),
        CardFactory.actions([
          {
            type: "imBack",
            title: "📝 Make a Post",
            value: "Make a Post",
          },
          {
            type: "imBack",
            title: "💬 Make a Comment",
            value: "Make a Comment",
          },
          {
            type: "imBack",
            title: "🙋🙋‍♂ Help",
            value: "Help",
          },
        ])
      );
      await stepContext.context.sendActivity({ attachments: [heroCard] });
    }
    return await stepContext.prompt("TextPrompt", promptMessage);
  }

  // Checking the user intent with Luis endpoint.
  async checkIntentStep(stepContext) {
    const context = stepContext.context;
    if (
      context.activity.type === "message" &&
      (context.activity.text !== undefined ||
        context.activity.value.text !== undefined)
    ) {
      const message = context.activity.text || context.activity.value.text;
      const { intent, score } = await getIntentAndScore(message);
      console.log(`Intent: ${intent} Score: ${score}`);

      if (intent === "Post.Feed" && score >= 0.7)
        return await stepContext.beginDialog("PostDialog", {});
      else if (intent === "Post.Comment" && score >= 0.7)
        return await stepContext.beginDialog("CommentDialog", {});
      else if (intent === "Utilities.Help" && score >= 0.7)
        return await stepContext.beginDialog("HelpDialog", {});
      else
        return await stepContext.replaceDialog(this.initialDialogId, {
          retry: true,
        });
    }
  }
}

module.exports.UserDialog = UserDialog;
