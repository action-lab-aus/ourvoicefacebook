const {
  ComponentDialog,
  ChoicePrompt,
  TextPrompt,
  ListStyle,
  WaterfallDialog,
} = require("botbuilder-dialogs");
const { CardFactory } = require("botbuilder");
const { ChoiceFactory } = require("botbuilder-choices");
const { getLocalDateTime } = require("../utils/timeFormatter");
const { generateName } = require("../utils/generateName");
const { getIntentAndScore } = require("../luis/luisHelper");
const {
  getReviews,
  getPost,
  deleteReview,
  postReview,
} = require("../apiGateway/apiGateway");

const { join } = require("path");
const ENV_FILE = join(__dirname, ".env");
require("dotenv").config({ path: ENV_FILE });
const graph = require("fbgraph");
const PAGE_TOKEN = process.env.FacebookPageToken;
// const USER_TOKEN = process.env.FacebookUserToken;
graph.setAccessToken(PAGE_TOKEN);

const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const MODERATION_DIALOG = "MODERATION_DIALOG";
const UPDATE_DIALOG = "UPDATE_DIALOG";
const MIN_REVIEW = 1;

const heroCard = CardFactory.heroCard(
  "Welcome to OurVoice!",
  "Provide moderations for anonymous posts or comments.",
  CardFactory.images([
    "https://firebasestorage.googleapis.com/v0/b/wechaty-demo.appspot.com/o/Images%2FAnonymous.png?alt=media&token=41df1eee-e1a4-4e4e-9551-a143d54ad319",
  ]),
  CardFactory.actions([
    {
      type: "imBack",
      title: "📝 Moderate Now",
      value: "Moderate Now",
    },
  ])
);

class AdminDialog extends ComponentDialog {
  constructor() {
    super("AdminDialog");

    this.addDialog(new TextPrompt("TextPrompt"));
    this.addDialog(new ChoicePrompt("ChoicePrompt"));

    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.initialStep.bind(this),
        this.checkIntentStep.bind(this),
      ])
    );

    this.addDialog(
      new WaterfallDialog(MODERATION_DIALOG, [
        this.showParagraphStep.bind(this),
        this.checkSelectionStep.bind(this),
        this.moderateStep.bind(this),
      ])
    );

    this.addDialog(
      new WaterfallDialog(UPDATE_DIALOG, [
        this.postReviewStep.bind(this),
        this.confirmStep.bind(this),
        this.reviewMoreStep.bind(this),
      ])
    );

    this.initialDialogId = WATERFALL_DIALOG;
    this.moderationDialogId = MODERATION_DIALOG;
    this.updateDialogId = UPDATE_DIALOG;
  }

  // Initial step for selecting the action to perform.
  async initialStep(stepContext) {
    // If user input is already Moderate.Post, then skip sending the hero card.
    if (stepContext.context.activity.text) {
      const { intent, score } = await getIntentAndScore(
        stepContext.context.activity.text
      );
      console.log(`Intent: ${intent} Score: ${score}`);

      if (intent === "Moderate.Post" && score >= 0.7) {
        return await stepContext.replaceDialog(this.moderationDialogId, {});
      }
    }

    var promptMessage;
    if (stepContext.options.retry) {
      promptMessage =
        "Sorry, I can't understand your message 😵 Can I say that again?";
    } else {
      promptMessage = "";
      await stepContext.context.sendActivity({ attachments: [heroCard] });
    }
    return await stepContext.prompt("TextPrompt", promptMessage);
  }

  // Checking the user intent with Luis endpoint.
  async checkIntentStep(stepContext) {
    const context = stepContext.context;
    if (
      context.activity.type === "message" &&
      (context.activity.text !== undefined ||
        context.activity.value.text !== undefined)
    ) {
      const message = context.activity.text || context.activity.value.text;
      const { intent, score } = await getIntentAndScore(message);
      console.log(`Intent: ${intent} Score: ${score}`);

      if (intent === "Moderate.Post" && score >= 0.7) {
        return await stepContext.replaceDialog(this.moderationDialogId, {});
      } else
        return await stepContext.replaceDialog(this.initialDialogId, {
          retry: true,
        });
    }
  }

  // Retrieve the post need to be moderated and show one paragraph at a time.
  async showParagraphStep(stepContext) {
    let postDetail = stepContext.options.postDetail;
    stepContext.values.commId = stepContext.options.commId;
    if (postDetail !== undefined) {
      stepContext.values.postDetail = stepContext.options.postDetail;
    } else {
      const getReviewRes = await getReviews();
      if (getReviewRes.status === 200) {
        const count = getReviewRes.headers.count;

        // Parse the post moderation details and find the next post to be moderated.
        const reviews = getReviewRes.data;
        const adminId = stepContext.context.activity.from.id;
        const { postId, commId } = findPostForModeration(reviews, adminId);

        if (postId === null) {
          await stepContext.context.sendActivity(
            "You have provided moderations for all current posts/comments! Please come back and check again later."
          );
          return await stepContext.endDialog();
        }

        const getPostRes = await getPost(postId);
        if (getPostRes.status !== 200) {
          console.error(getPostRes.data);
          await stepContext.context.sendActivity(
            "Sorry, an unexpected error occurred when checking the moderation status, please try again later 😵"
          );
          return await stepContext.endDialog();
        }

        postDetail = getPostRes.data;
        stepContext.values.postDetail = postDetail;

        var summaryMsg;
        if (commId) {
          stepContext.values.commId = commId;
          summaryMsg = `Sure, currently we have ${count} post/comment${
            count > 1 ? "s" : ""
          } which require${
            count > 1 ? "" : "s"
          } further moderation. I will now start to show you the first comment. This comment was submitted on ${
            postDetail.postDate
          }`;
        } else {
          summaryMsg = `Sure, currently we have ${count} post/comment${
            count > 1 ? "s" : ""
          } which require${
            count > 1 ? "" : "s"
          } further moderation. I will now start to show you the first post one paragraph at a time. This post was submitted on ${
            postDetail.postDate
          }, with category ${postDetail.category.join(", ")}.`;
        }

        // Provide a short summary of the moderation status and post details.
        await stepContext.context.sendActivity(summaryMsg);
      } else {
        console.error(getReviewRes.data);
        await stepContext.context.sendActivity(
          "Sorry, an unexpected error occurred when checking the moderation status, please try again later 😵"
        );
        return await stepContext.endDialog();
      }
    }

    stepContext.values.moderation = stepContext.options.moderation || [];
    const moderation = stepContext.values.moderation;
    const commId = stepContext.values.commId;
    const index = moderation.length;

    var heroCardTitle;
    var heroCardSubtitle;
    if (commId) {
      const comment = postDetail.comments.find((c) => c.commId === commId);
      heroCardTitle = `Comment Content${
        comment.moderations.length === 0 ? "" : " (Moderated)"
      }`;
      heroCardSubtitle = comment.commContent;
    } else {
      heroCardTitle = `Paragraph ${index + 1}/${postDetail.content.length}${
        postDetail.moderations.length === 0 ? "" : " (Moderated)"
      }`;
      heroCardSubtitle =
        postDetail.moderations.length === 0
          ? postDetail.content[index]
          : postDetail.moderations[postDetail.moderations.length - 1].content[
              index
            ];
    }

    return await stepContext.prompt("ChoicePrompt", {
      prompt: `${heroCardTitle}:\n\n${heroCardSubtitle}`,
      choices: ChoiceFactory.toChoices([
        "Moderate Paragraph",
        "Approve Paragraph",
      ]),
      style: ListStyle.suggestedAction,
    });
  }

  // Check the user selection for moderation or approve.
  async checkSelectionStep(stepContext) {
    const index = stepContext.result.index;
    const commId = stepContext.values.commId;
    const postDetail = stepContext.values.postDetail;
    let moderation = [...stepContext.values.moderation];

    if (index === 1) {
      if (!commId) {
        const newModerContent =
          postDetail.moderations.length === 0
            ? postDetail.content[moderation.length]
            : postDetail.moderations[postDetail.moderations.length - 1].content[
                moderation.length
              ];
        moderation.push(newModerContent);

        if (moderation.length < postDetail.content.length)
          return await stepContext.replaceDialog(this.moderationDialogId, {
            postDetail: postDetail,
            moderation: moderation,
          });
        else
          return await stepContext.replaceDialog(this.updateDialogId, {
            postDetail: postDetail,
            moderation: moderation,
          });
      } else {
        const comment = postDetail.comments.find((c) => c.commId === commId);
        moderation.push(comment.commContent);

        return await stepContext.replaceDialog(this.updateDialogId, {
          postDetail: postDetail,
          moderation: moderation,
          commId: commId,
        });
      }
    } else {
      return await stepContext.prompt(
        "TextPrompt",
        "Please modify the paragraph and send back to me when you finish."
      );
    }
  }

  // Update the moderation content list based on user selection.
  async moderateStep(stepContext) {
    const newContent = stepContext.result;
    const postDetail = stepContext.values.postDetail;
    const commId = stepContext.values.commId;
    let moderation = [...stepContext.values.moderation];
    moderation.push(newContent);

    if (!commId) {
      if (moderation.length < postDetail.content.length)
        return await stepContext.replaceDialog(this.moderationDialogId, {
          postDetail: postDetail,
          moderation: moderation,
        });
      else
        return await stepContext.replaceDialog(this.updateDialogId, {
          postDetail: postDetail,
          moderation: moderation,
        });
    } else {
      return await stepContext.replaceDialog(this.updateDialogId, {
        postDetail: postDetail,
        moderation: moderation,
        commId: commId,
      });
    }
  }

  // Post the moderation result.
  async postReviewStep(stepContext) {
    const postDetail = stepContext.options.postDetail;
    stepContext.values.postDetail = postDetail;
    const moderation = stepContext.options.moderation;
    stepContext.values.moderation = moderation;
    const commId = stepContext.options.commId;
    stepContext.values.commId = commId;

    // Provide the post moderated content summary.
    await stepContext.context.sendActivity(
      `This is the moderated content summary:\r\n\n${moderation.join("\r\n\n")}`
    );

    return await stepContext.prompt("ChoicePrompt", {
      prompt: "Do you want to submit your moderation now?",
      choices: ChoiceFactory.toChoices([
        "Yes!",
        "No, try again...",
        "Cancel...",
      ]),
      style: ListStyle.suggestedAction,
    });
  }

  async confirmStep(stepContext) {
    const postDetail = stepContext.values.postDetail;
    const moderation = stepContext.values.moderation;
    const commId = stepContext.values.commId;

    const newModeration = {
      moderator: stepContext.context.activity.from.id,
      moderDate: getLocalDateTime(),
      content: moderation,
    };

    const index = stepContext.result.index;
    switch (index) {
      case 0: {
        if (!commId) {
          console.log("Enter Publish Post...");
          const postReviewRes = await postReview(
            postDetail.postId,
            newModeration
          );

          if (postReviewRes.status !== 200) {
            console.error(postReviewRes.data);
            await stepContext.context.sendActivity(
              "Sorry, an unexpected error occurred when updating moderation details, please try again later 😵"
            );
            return await stepContext.endDialog();
          }
        } else {
          console.log("Enter Publish Comment...");
          const commIndex = findCommentIndex(postDetail.comments, commId);

          console.log(commIndex);
          const postReviewRes = await postReview(
            postDetail.postId,
            newModeration,
            commId,
            commIndex
          );

          if (postReviewRes.status === 200) {
            // Check the number of moderations for the current post.
            // If reachs minimum moderation requirement, then publish the post to Facebook.
            const postContent = postReviewRes.data.comments.find(
              (c) => c.commId === commId
            );
            if (postContent.moderations.length === MIN_REVIEW) {
              // Create a new pure text post POST /page-id/feed.
              const name = generateName();
              const message = `Posted by ${name}: ${
                postContent.moderations[postContent.moderations.length - 1]
                  .content
              }`;
              const facebookId = postReviewRes.data.facebookId;

              graph.post(
                `/${facebookId}/comments`, // Live Facebook Page
                { message: message },
                async function (err, res) {
                  if (err) console.error(err);
                  console.log("Post comment response: ", res);

                  const deleteRes = await deleteReview(
                    postDetail.postId,
                    commId
                  );
                  if (deleteRes.status !== 200) {
                    console.error(deleteRes.data);
                    await stepContext.context.sendActivity(
                      "Sorry, an unexpected error occurred when updating moderation details, please try again later 😵"
                    );
                    return await stepContext.endDialog();
                  }
                }
              );
            }
          } else {
            console.error(postReviewRes.data);
            await stepContext.context.sendActivity(
              "Sorry, an unexpected error occurred when updating moderation details, please try again later 😵"
            );
            return await stepContext.endDialog();
          }
        }

        return await stepContext.prompt("ChoicePrompt", {
          prompt:
            "Thanks, your review has been recorded. Do you want to provide more moderations now?",
          choices: ChoiceFactory.toChoices(["Yes!", "No..."]),
          style: ListStyle.suggestedAction,
        });
      }

      case 1:
        return await stepContext.replaceDialog(this.moderationDialogId, {
          postDetail: postDetail,
          commId: commId,
        });

      case 2:
        await stepContext.context.sendActivity(
          "Cheers, click the `Moderate Now` button below again if you want to provide more moderations!"
        );
        await stepContext.context.sendActivity({ attachments: [heroCard] });
        return await stepContext.endDialog();
    }
  }

  // Confirm with admin users whether they want to perform more moderations now.
  async reviewMoreStep(stepContext) {
    const index = stepContext.result.index;
    if (index === 0)
      return await stepContext.replaceDialog(this.moderationDialogId, {});
    else {
      await stepContext.context.sendActivity(
        "Cheers, click the `Moderate Now` button below again if you want to provide more moderations!"
      );
      await stepContext.context.sendActivity({ attachments: [heroCard] });
      return await stepContext.endDialog();
    }
  }
}

function findPostForModeration(reviews, adminId) {
  for (let i = 0; i < reviews.length; i++) {
    if (!reviews[i].moderatedBy.includes(adminId))
      return {
        postId: reviews[i].postId,
        commId: reviews[i].commId !== "null" ? reviews[i].commId : undefined,
      };
  }
  return { postId: null };
}

function findCommentIndex(comments, commId) {
  console.log(comments);
  console.log(commId);
  for (let i = 0; i < comments.length; i++) {
    if (comments[i].commId === commId) return i;
  }
  return -1;
}

module.exports.AdminDialog = AdminDialog;
