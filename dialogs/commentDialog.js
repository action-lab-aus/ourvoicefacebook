const {
  ComponentDialog,
  ChoicePrompt,
  TextPrompt,
  ListStyle,
  WaterfallDialog,
} = require("botbuilder-dialogs");
const { getLocalDateTime } = require("../utils/timeFormatter");
const { ChoiceFactory } = require("botbuilder-choices");
const { getPost, postComment } = require("../apiGateway/apiGateway");

const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const COMMENT_DIALOG = "COMMENT_DIALOG";

class CommentDialog extends ComponentDialog {
  constructor() {
    super("CommentDialog");

    this.addDialog(new TextPrompt("TextPrompt"));
    this.addDialog(new ChoicePrompt("ChoicePrompt"));

    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.initialStep.bind(this),
        this.getPostStep.bind(this),
      ])
    );

    this.addDialog(
      new WaterfallDialog(COMMENT_DIALOG, [
        this.askCommentStep.bind(this),
        this.getCommentStep.bind(this),
        this.confirmStep.bind(this),
        this.moreCommentStep.bind(this),
      ])
    );

    this.initialDialogId = WATERFALL_DIALOG;
    this.commentDialogId = COMMENT_DIALOG;
  }

  // Ask the user for the post id for commenting.
  async initialStep(stepContext) {
    var promptMessage;
    if (stepContext.options.retry)
      promptMessage =
        "Sorry, the Post ID you entered is invalid 😵 Could you please check and share it with me again?";
    else promptMessage = "What is the Post ID that you want to comment on?";

    return await stepContext.prompt("TextPrompt", promptMessage);
  }

  // Retrieve the post details for commenting.
  async getPostStep(stepContext) {
    const postId = stepContext.result;
    const getPostRes = await getPost(postId);

    if (getPostRes.status !== 200)
      return await stepContext.replaceDialog(this.initialDialogId, {
        retry: true,
      });

    const postDetail = getPostRes.data;
    stepContext.values.postDetail = postDetail;

    // If no facebook id, then the post hasn't been published
    if (!postDetail.facebookId)
      return await stepContext.replaceDialog(this.initialDialogId, {
        retry: true,
      });

    // Provide a short summary of the post details.
    await stepContext.context.sendActivity(
      `This post was submitted on ${
        postDetail.postDate
      }, with category ${postDetail.category.join(
        ", "
      )}. Here are the post details:`
    );

    // Provide the post content.
    await stepContext.context.sendActivity(
      `${postDetail.content.join("\r\n\n")}`
    );

    return await stepContext.replaceDialog(this.commentDialogId, {
      postDetail: postDetail,
    });
  }

  // Asking users for the comment they want to provide.
  async askCommentStep(stepContext) {
    stepContext.values.postDetail = stepContext.options.postDetail;

    var promptMessage;
    if (stepContext.options.retry)
      promptMessage = "Sure, can you provide me with the comment again?";
    else promptMessage = "What comment do you want to make for this post?";

    return await stepContext.prompt("TextPrompt", promptMessage);
  }

  // Ask for comment confirmation from the user.
  async getCommentStep(stepContext) {
    const comment = stepContext.result;
    stepContext.values.comment = comment;

    return await stepContext.prompt("ChoicePrompt", {
      prompt: "Thanks, are you sure this is the comment that you want to post?",
      choices: ChoiceFactory.toChoices([
        "Yes, that's it!",
        "No, try again...",
        "Cancel...",
      ]),
      style: ListStyle.suggestedAction,
    });
  }

  // Post comments if user confirms.
  async confirmStep(stepContext) {
    const index = stepContext.result.index;

    switch (index) {
      case 0: {
        const postId = stepContext.values.postDetail.postId;
        const date = new Date();
        const comment = {
          commId: `C-${date.getTime()}`,
          commDate: getLocalDateTime(),
          commBy: stepContext.context.activity.from.id,
          commContent: stepContext.values.comment,
        };

        const postCommRes = await postComment(postId, comment);
        if (postCommRes.status === 200) {
          return await stepContext.prompt("ChoicePrompt", {
            prompt:
              "Cheers, your comment will be reviewed and scheduled before publishing to the Facebook Page anonymously! Is there anything else that I can do for you?",
            choices: ChoiceFactory.toChoices([
              "Comment same post",
              "Comment new post",
              "No, that's all...",
            ]),
            style: ListStyle.suggestedAction,
          });
        } else {
          console.error(postCommRes.data);
          await stepContext.context.sendActivity(
            "Sorry, an unexpected error occurred when recording your comment, please try again later 😵 If the error persists to occur, please contact the administrator for help."
          );
          return await stepContext.endDialog();
        }
      }

      case 1:
        return await stepContext.replaceDialog(this.commentDialogId, {
          retry: true,
          postDetail: stepContext.values.postDetail,
        });

      case 2:
        await stepContext.context.sendActivity(
          "Sure, chat with me again if you have changed your mind."
        );
        return await stepContext.endDialog();
    }

    return await stepContext.endDialog();
  }

  // Asking user whether they want to provide more comments.
  async moreCommentStep(stepContext) {
    const index = stepContext.result.index;

    switch (index) {
      case 0:
        return await stepContext.replaceDialog(this.commentDialogId, {
          postDetail: stepContext.values.postDetail,
        });

      case 1:
        return await stepContext.replaceDialog(this.initialDialogId, {});

      case 2:
        await stepContext.context.sendActivity(
          "Thanks, please feel free to chat with me again if you want to make more anonymous posts/comments."
        );
        return await stepContext.endDialog();
    }
  }
}

module.exports.CommentDialog = CommentDialog;
