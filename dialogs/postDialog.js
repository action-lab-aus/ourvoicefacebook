const {
  ComponentDialog,
  TextPrompt,
  ChoicePrompt,
  ListStyle,
  WaterfallDialog,
} = require("botbuilder-dialogs");
const { CardFactory } = require("botbuilder");
const { generateSummaryCard } = require("./postSummaryCard");
const { getLocalDateTime } = require("../utils/timeFormatter");
const { postPost } = require("../apiGateway/apiGateway");
const { ChoiceFactory } = require("botbuilder-choices");

const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const CONTENT_DIALOG = "CONTENT_DIALOG";

class PostDialog extends ComponentDialog {
  constructor() {
    super("PostDialog");

    this.addDialog(new TextPrompt("TextPrompt"));
    this.addDialog(new ChoicePrompt("ChoicePrompt"));

    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.addLabelStep.bind(this),
        this.moreLabelStep.bind(this),
      ])
    );

    this.addDialog(
      new WaterfallDialog(CONTENT_DIALOG, [
        this.addContentStep.bind(this),
        this.moreContentStep.bind(this),
        this.confirmStep.bind(this),
      ])
    );

    this.initialDialogId = WATERFALL_DIALOG;
    this.contentDialogId = CONTENT_DIALOG;
  }

  // Process the user label selection.
  async addLabelStep(stepContext) {
    const retry = stepContext.options.retry;

    var labelList;
    if (stepContext.options.labelList) {
      labelList = stepContext.options.labelList;
      labelList.push("No, that's all!");
    } else
      labelList = ["Workplace Safety", "Harassment", "Colleague", "Salary"];
    stepContext.values.labelList = labelList;

    var labels;
    if (stepContext.options.labels) labels = stepContext.options.labels;
    else labels = [];
    stepContext.values.labels = labels;

    return await stepContext.prompt("ChoicePrompt", {
      prompt: retry
        ? "Is there any other categories you would like to add?"
        : "OurVoice is completely anonymous and all posts and comments are anonymously moderated by volunteers. What is your new post about?",
      choices: ChoiceFactory.toChoices(labelList),
      style: ListStyle.suggestedAction,
    });
  }

  // Ask for adding more labels from the user.
  async moreLabelStep(stepContext) {
    const message = stepContext.values.message;
    const postDate = stepContext.values.postDate;
    var labelList = stepContext.values.labelList;
    var labels = stepContext.values.labels;

    const result = stepContext.result;
    const stopFlag = result.index !== labelList.length - 1;

    if (stopFlag) {
      const label = labelList.splice(result.index, 1)[0];
      labels.push(label);
      if (labels.length !== 1)
        labelList = labelList.splice(0, --labelList.length);
    }

    // If user choose to add more labels and less than the limited number, then
    // ask the user to select more labels.
    if (labels.length < 3 && stopFlag)
      return await stepContext.replaceDialog(this.initialDialogId, {
        retry: true,
        message: message,
        postDate: postDate,
        labelList: labelList,
        labels: labels,
      });
    else
      return await stepContext.replaceDialog(this.contentDialogId, {
        labels: labels,
      });
  }

  // Process the content of the new post.
  async addContentStep(stepContext) {
    stepContext.values.labels = stepContext.options.labels;
    stepContext.values.content = stepContext.options.content || [];
    const retry = stepContext.options.retry;

    const promptMessage = retry
      ? ""
      : "Could you please send me the message you want to post anonymously? If the content is very long, try to separate them into small paragraphs. Reply me with `Finish` to wrap up your message.";

    return await stepContext.prompt("TextPrompt", promptMessage);
  }

  // Confirm whether more contents will be entered.
  async moreContentStep(stepContext) {
    const message = stepContext.result;
    const content = stepContext.values.content;
    const labels = stepContext.values.labels;

    if (message.toLowerCase().trim() === "finish") {
      // Provide the summary of the new post.
      const summaryJson = generateSummaryCard(content.join("\r\n"), labels);
      const summaryCard = CardFactory.adaptiveCard(summaryJson);
      await stepContext.context.sendActivity({ attachments: [summaryCard] });
      return await stepContext.prompt("ChoicePrompt", {
        prompt:
          "You will be provided with a summary of your anonymous post details within 2 minutes. Please review the summary and confirm whether it is the post you want to publish anonymously.",
        choices: ChoiceFactory.toChoices([
          "Yes, that's it!",
          "No, try again...",
          "Cancel...",
        ]),
        style: ListStyle.suggestedAction,
      });
    } else {
      content.push(message);
      return await stepContext.replaceDialog(this.contentDialogId, {
        retry: true,
        content: content,
        labels: stepContext.values.labels,
      });
    }
  }

  // Confirm whether the post content is correct.
  async confirmStep(stepContext) {
    const index = stepContext.result.index;

    switch (index) {
      case 0: {
        const content = stepContext.values.content;
        const labels = stepContext.values.labels;

        // Post the message to DynamoDB.
        const post = {
          postDate: getLocalDateTime(),
          category: labels,
          postedBy: stepContext.context.activity.from.id,
          content: content,
        };

        const postRes = await postPost(post);
        if (postRes.status === 201)
          await stepContext.context.sendActivity(
            "I have recorded all the information for your post 🤐 Your post will be reviewed and scheduled before publishing to the Facebook Page anonymously. Chat with me again if you want to explore other features!"
          );
        else {
          console.error(postRes.data);
          await stepContext.context.sendActivity(
            "Sorry, an unexpected error occurred when recording your post, please try again later 😵 If the error persists to occur, please contact the administrator for help."
          );
        }
        return await stepContext.endDialog();
      }

      case 1:
        return await stepContext.replaceDialog(this.initialDialogId, {});

      case 2:
        await stepContext.context.sendActivity(
          "Sure, chat with me again if you have changed your mind."
        );
        return await stepContext.endDialog();
    }
  }

  async promptValidator(promptContext) {
    const activity = promptContext.context.activity;
    return activity.type === "message" && activity.channelData.postBack;
  }
}

module.exports.PostDialog = PostDialog;
