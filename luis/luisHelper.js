const { getLuisResponse } = require("./luisEndpoint");

/**
 * Get the intent and score by calling the luis endpoint.
 * @param {String} message Message to be checked.
 */
async function getIntentAndScore(message) {
  try {
    // Get luis response by forwarding the user message.
    const luisResponse = JSON.parse(await getLuisResponse(message));

    // Calculate the top scoring intent with the corresponding score.
    const intent = luisResponse.prediction.topIntent;
    const score = luisResponse.prediction.intents[intent].score;

    // If the score is high enough, then parse the response and fulfill user request.
    return { intent: intent, score: score };
  } catch (error) {
    console.error(error);
    return { intent: "None", score: 0.0 };
  }
}

exports.getIntentAndScore = getIntentAndScore;
