const querystring = require("querystring");
const request = require("requestretry");
const https = require("https");

// Time delay between requests.
const delayMS = 5000;
// Retry recount.
const retry = 10;

// Retry reqeust if error or 429 received.
var retryStrategy = function(err, response, body) {
  let shouldRetry = err || response.statusCode === 429;
  if (shouldRetry) console.log("Retry...");
  return shouldRetry;
};

var httpAgent = new https.Agent();
httpAgent.maxSockets = 1;

// For production environment, remove the string here
// LUIS application ID.
const APP_ID = process.env.LuisAppId;
// LUIS endpoint key.
const ENDPOINT_KEY = process.env.LuisAPIKey;
// LUIS application region.
const REGION = "westus";

/**
 * Send query to the Luis endpoint through http request.
 * @param {String} utterance The user utterance to be evaluated.
 */
async function sendQueryToEndpoint(utterance) {
  var queryParams = querystring.stringify({
    "subscription-key": ENDPOINT_KEY,
    verbose: true,
    query: utterance
  });

  var luisRequest = `https://${REGION}.api.cognitive.microsoft.com/luis/prediction/v3.0/apps/${APP_ID}/slots/production/predict?${queryParams}`;

  // Additional options allow request to manage F0 pricing tier limitations.
  var options = {
    uri: luisRequest,
    headers: {
      Connection: "keep-alive"
    },
    maxAttempts: retry,
    retryDelay: delayMS,
    retryStrategy: retryStrategy,
    resolveWithFullResponse: true,
    timeout: 60000,
    pool: httpAgent
  };

  return await request.get(options);
}

/**
 * Send query to the Luis endpoint through http request with try-catch block.
 * @param {String} utterance The user utterance to be evaluated.
 */
async function getLuisResponse(utterance) {
  try {
    var queryResponse = await sendQueryToEndpoint(utterance);
    return queryResponse.body;
  } catch (error) {
    console.error(error);
  }
}

exports.getLuisResponse = getLuisResponse;
